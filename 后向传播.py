# encoding: utf-8

# !/usr/bin/env python

import tensorflow as tf

import numpy as np

BATCH_SIZE = 8
seed = 23455

rng = np.random.RandomState(seed)
X = rng.rand(32, 2)

Y = [[int(x0 + x1 < 1)] for (x0, x1) in X]

print X
print Y

# 神经网络的输入，参数和输出，前向传播过程


# 输入参数
x = tf.placeholder(tf.float32, shape=(None, 2))
y_ = tf.placeholder(tf.float32, shape=(None, 1))

# 第一二层网络参数
w1 = tf.Variable(tf.random_normal([2, 3], stddev=1, seed=1))
w2 = tf.Variable(tf.random_normal([3, 1], stddev=1, seed=1))

# 推理过程，矩阵乘法
a = tf.matmul(x, w1)
y = tf.matmul(a, w2)

# 损失函数和反向传播方法

loss = tf.reduce_mean(tf.square(y - y_))

train_step = tf.train.GradientDescentOptimizer(0.001).minimize(loss)

with tf.Session() as sess:
    init_op = tf.global_variables_initializer()
    sess.run(init_op)
    print sess.run(w1)
    print sess.run(w2)

    STEPS = 3000
    for i in range(STEPS):
        start = (i * BATCH_SIZE) % 32
        end = start + BATCH_SIZE
        sess.run(train_step, feed_dict={x: X[start:end], y_: Y[start:end]})

        if i % 500 == 0:
            total_loss = sess.run(loss, feed_dict={x: X, y_: Y})
            print '{}{}'.format(i, total_loss)
    print sess.run(w1)
    print sess.run(w2)

"""
神经网络八股
准备参数
前向传播
反向传播
迭代
"""
