# encoding: utf-8

# !/usr/bin/env python

import tensorflow as tf

import numpy as np

BATCH_SIZE = 8
seed = 23455
COST = 9
PROFIT = 1

rng = np.random.RandomState(seed)
X = rng.rand(32, 2)

Y_ = [[x1 + x2 + (rng.rand() / 10.0 - 0.05)] for (x1, x2) in X]

# 神经网络的输入，参数和输出，前向传播过程


# 输入参数
x = tf.placeholder(tf.float32, shape=(None, 2))
y_ = tf.placeholder(tf.float32, shape=(None, 1))

# 第一二层网络参数
w1 = tf.Variable(tf.random_normal([2, 1], stddev=1, seed=1))

# 推理过程，矩阵乘法
y = tf.matmul(x, w1)

# 损失函数和反向传播方法

loss_mse = tf.reduce_sum(tf.where(tf.greater(y, y_), (y - y_) * COST, (y_ - y)*PROFIT))

train_step = tf.train.GradientDescentOptimizer(0.001).minimize(loss_mse)

with tf.Session() as sess:
    init_op = tf.global_variables_initializer()
    sess.run(init_op)

    STEPS = 20000
    for i in range(STEPS):
        start = (i * BATCH_SIZE) % 32
        end = start + BATCH_SIZE
        sess.run(train_step, feed_dict={x: X[start:end], y_: Y_[start:end]})

        if i % 500 == 0:
            print '{}结果{}'.format(i, sess.run(w1))
    print sess.run(w1)
