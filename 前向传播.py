# encoding: utf-8

# !/usr/bin/env python

import tensorflow as tf

# 定义输入和参数
X = tf.constant([[0.7, 0.5]])

w1 = tf.Variable(tf.random_normal([2, 3], stddev=1, seed=1))
w2 = tf.Variable(tf.random_normal([3, 1], stddev=1, seed=1))

# 前向传播
# 矩阵乘法
a = tf.matmul(X, w1)
y = tf.matmul(a, w2)

# 会话计算
with tf.Session() as sess:
    init_op = tf.global_variables_initializer()
    sess.run(init_op)

    print sess.run(y)
