# encoding: utf-8

# !/usr/bin/env python


import tensorflow as tf

a = tf.constant([1.0, 2.0])

b = tf.constant([3.0, 4.0])

res = a + b

print res

x = tf.constant([[1.0, 2.0]])
w = tf.constant([[3.0], [4.0]])

y = tf.matmul(x, w)

print y

# 变量初始化 计算图节点都要用with结构
with tf.Session() as sess:
    print sess.run(y)


z=tf.Variable(tf.random_normal([2,3],stddev=2,mean=0,seed=1))

print z




